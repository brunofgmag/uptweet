FROM rust:1.54-slim-buster

RUN USER=root cargo new --bin uptweet
WORKDIR /uptweet

COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
COPY ./Rocket.toml ./Rocket.toml
COPY ./migrations ./migrations
COPY ./.env.example ./.env

RUN apt update
RUN apt install -y sqlite3 libsqlite3-dev
RUN cargo install diesel_cli --no-default-features --features sqlite
RUN diesel setup
RUN cargo build --release
RUN rm src/*.rs

COPY ./src ./src

RUN rm ./target/release/deps/uptweet*
RUN cargo build --release

CMD ["./target/release/uptweet"]
