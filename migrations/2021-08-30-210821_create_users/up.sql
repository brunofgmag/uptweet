CREATE TABLE "users" (
    `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    `name` VARCHAR NOT NULL,
    `username` VARCHAR UNIQUE NOT NULL,
    `password` VARCHAR NOT NULL,
    `auth_token` VARCHAR UNIQUE
);

INSERT INTO "users" (
    `name`, `username`, `password`
) VALUES ("Bruno", "brunofgmag", "123456");