CREATE TABLE "tweets" (
    `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    `content` VARCHAR NOT NULL,
    `user_id` INTEGER NOT NULL,
    `tweet_id` INTEGER,

    FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE
    FOREIGN KEY (`tweet_id`) REFERENCES tweets(`id`) ON DELETE CASCADE
);