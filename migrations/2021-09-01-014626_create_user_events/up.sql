CREATE TABLE "user_events" (
    `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    `causer_id` INTEGER NOT NULL,
    `target_id` INTEGER NOT NULL,
    `event_type` VARCHAR NOT NULL,

    FOREIGN KEY (`causer_id`) REFERENCES users(`id`) ON DELETE CASCADE
    FOREIGN KEY (`target_id`) REFERENCES users(`id`) ON DELETE CASCADE
);