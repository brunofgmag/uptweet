## UpTweet

UpTweet is a simple tweet API made with Rust/Rocket/Diesel.

### Requirements

* docker

### Documentation

The api was documented on the OpenAPI 3.0.0 standard.

- **[UpTweet API Documentation](https://app.swaggerhub.com/apis/brunofgmag1/UpTweet/1)**

### Quick Start

Build the docker container using the following command:

`docker build -t uptweet .`

Wait for the build process to finish and then run:

`docker run -p 8000:8000 --rm --name uptweet1 uptweet`

### Authentication

The API uses a simple bearer token authorization, after the login, the API will return a token on the response body that must be put on the `Authorization` header in the following format:

`Basic put_token_here`

You can login with the default user:

1. Username: `brunofgmag`
2. Password: `123456`
