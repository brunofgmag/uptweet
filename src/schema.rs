table! {
    likes (id) {
        id -> Integer,
        tweet_id -> Integer,
        user_id -> Integer,
    }
}

table! {
    tweets (id) {
        id -> Integer,
        content -> Text,
        user_id -> Integer,
        tweet_id -> Nullable<Integer>,
    }
}

table! {
    user_events (id) {
        id -> Integer,
        causer_id -> Integer,
        target_id -> Integer,
        event_type -> Text,
    }
}

table! {
    users (id) {
        id -> Integer,
        name -> Text,
        username -> Text,
        password -> Text,
        auth_token -> Nullable<Text>,
    }
}

joinable!(likes -> tweets (tweet_id));
joinable!(likes -> users (user_id));
joinable!(tweets -> users (user_id));

allow_tables_to_appear_in_same_query!(
    likes,
    tweets,
    user_events,
    users,
);
