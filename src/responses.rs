use rocket::serde::{json::Value, json::serde_json::json};
use rocket::response::status;
use rocket::http::Status;

#[derive(Serialize, Deserialize)]
pub struct JsonResponse {
    pub status: bool,
    pub code: Option<i32>,
    pub message: String,
    pub payload: Option<Value>
}

#[derive(Serialize, Deserialize)]
pub struct AuthResponseBody {
    pub token: String
}

pub struct ErrorResponse;

impl ErrorResponse {
    pub fn create(status: Status, code: i32, message: &str, payload: Option<Value>) -> status::Custom<Value>
    {
        status::Custom(status, JsonResponse::create(
            false,
            Some(code),
            String::from(message),
            payload
        ))
    }
}

impl JsonResponse {
    pub fn create(status: bool, code: Option<i32>, message: String, payload: Option<Value>) -> Value
    {
        json!(
            JsonResponse {
                status,
                code,
                message,
                payload
            }
        )
    }
}