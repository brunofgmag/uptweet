use crate::schema::likes;

#[derive(Serialize, Queryable)]
pub struct Like {
    pub id: i32,
    pub tweet_id: i32,
    pub user_id: i32
}

#[derive(Insertable, Deserialize)]
#[table_name = "likes"]
pub struct NewLike {
    pub tweet_id: i32
}