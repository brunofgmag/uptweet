use rocket::serde::{json::Value, json::serde_json::json};
use crate::schema::tweets;

#[derive(Serialize, Queryable, Debug)]
pub struct Tweet {
    pub id: i32,
    pub content: String,
    pub user_id: i32,
    pub tweet_id: Option<i32>
}

#[derive(Serialize)]
pub struct TweetJson {
    pub id: i32,
    pub content: String,
    pub user_id: i32,
    pub tweet_id: Option<i32>,
    pub likes: usize,
    pub comments: Option<Vec<TweetJson>>
}

#[derive(Insertable, Deserialize)]
#[table_name = "tweets"]
pub struct NewTweet {
    pub content: String,
    pub tweet_id: Option<i32>
}

impl TweetJson {
    pub fn create(id: i32, content: String, user_id: i32, tweet_id: Option<i32>, likes: usize, comments: Option<Vec<TweetJson>>) -> Value
    {
        json!(
            TweetJson {
                id,
                content,
                user_id,
                tweet_id,
                likes,
                comments
            }
        )
    }
}