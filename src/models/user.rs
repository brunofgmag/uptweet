use rocket::serde::{json::Value, json::serde_json::json};
use crate::schema::users;

#[derive(Serialize, Deserialize, Queryable, Debug)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub username: String,
    #[serde(skip_serializing)]
    pub password: String,
    #[serde(skip_serializing)]
    pub auth_token: Option<String>
}

#[derive(Insertable, Deserialize)]
#[table_name = "users"]
pub struct NewUser {
    pub name: String,
    pub username: String,
    pub password: String,
}

#[derive(Serialize, Deserialize)]
pub struct UserAuth {
    pub username: String,
    pub password: String
}

#[derive(Serialize)]
pub struct UserJson {
    pub id: i32,
    pub name: String,
    pub username: String,
    pub followers: usize
}

impl UserJson {
    pub fn create(id: i32, name: String, username: String, followers: usize) -> Value
    {
        json!(
            UserJson {
                id,
                name,
                username,
                followers
            }
        )
    }
}