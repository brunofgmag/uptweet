use diesel::prelude::*;
use crate::schema::user_events;

#[derive(Serialize, Deserialize, Queryable, Identifiable, Debug)]
pub struct UserEvent {
    pub id: i32,
    pub causer_id: i32,
    pub target_id: i32,
    pub event_type: String,
}

#[derive(Serialize, Deserialize, Insertable, Debug)]
#[table_name = "user_events"]
pub struct NewUserEvent {
    pub target_id: i32,
    pub event_type: String,
}

impl UserEvent {
    pub fn exists(c: &SqliteConnection, causer_id: i32, target_id: i32, event_type: String) -> (i32, bool)
    {
        let result = user_events::table
            .filter(user_events::causer_id.eq(causer_id))
            .filter(user_events::target_id.eq(target_id))
            .filter(user_events::event_type.eq(event_type))
            .first::<UserEvent>(c);

        match result {
            Ok(event) => (event.id, true),
            Err(_) => (0, false)
        }
    }
}