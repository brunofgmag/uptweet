use rocket::serde::{json::Value, json::serde_json::json};
use rocket::response::status;
use rocket::http::Status;
use wyhash::wyrng;
use rand::Rng;
use diesel::prelude::*;
use crate::responses::{JsonResponse, AuthResponseBody};
use crate::schema::users;
use crate::models::user::*;
use crate::DbConn;

pub struct LoginController;

impl LoginController {
    pub async fn login(user_auth: UserAuth, conn: DbConn) -> Result<Value, status::Custom<Value>>
    {
        conn.run(|c| {
            let username = user_auth.username;
            let password = user_auth.password;
            let mut response = JsonResponse {
                status: false,
                code: Some(404),
                message: String::from("Login information not found."),
                payload: None
            };

            let result = Self::check(username, password, c);

            match result {
                Ok(hash) => {
                    response = JsonResponse {
                        status: true,
                        code: Some(200),
                        message: String::from("OK"),
                        payload: Some(json!(AuthResponseBody {
                            token: hash
                        }))
                    };
    
                    Ok(json!(response))
                },
                Err(_) => {
                    Err(status::Custom(Status::NotFound, json!(response)))
                }
            }
        }).await
    }

    fn check(username: String, password: String, c: &SqliteConnection) -> Result<String, bool>
    {
        let result = users::table
            .filter(users::username.eq(username))
            .filter(users::password.eq(password))
            .first::<User>(c);

        match result {
            Ok(user) => {
                let mut rng = rand::thread_rng();
                let hash = format!("{:x}", wyrng(&mut rng.gen::<u64>()));

                Self::update_token(user, &hash, c);

                Ok(hash)
            },
            Err(_) => Err(false),
        }
    }

    fn update_token(user: User, hash: &String, c: &SqliteConnection)
    {
        diesel::update(users::table.find(user.id))
            .set(users::auth_token.eq(hash))
            .execute(c)
            .expect("Error updating token!");
    }
}
