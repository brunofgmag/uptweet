use rocket::serde::{json::Json, json::Value, json::serde_json::json};
use rocket::response::status;
use crate::models::{user::NewUser, user_event::NewUserEvent};
use crate::repositories::user_repository::UserRepository;
use crate::DbConn;

pub struct UserController;

impl UserController {
    pub async fn get(user_id: i32, conn: DbConn) -> Result<Value, status::Custom<Value>>
    {
        conn.run(move |c| {
            UserRepository::find(user_id, c)
                .map(|response| json!(response))
                .map_err(|e| e)
        }).await
    }

    pub async fn create(conn: DbConn, new_user: Json<NewUser>) -> Result<Value, status::Custom<Value>>
    {
        conn.run(move |c| {
            UserRepository::create(c, new_user.into_inner())
                .map(|response| json!(response))
                .map_err(|e| e)
        }).await
    }

    pub async fn block(current_user: i32, conn: DbConn, new_user_event: Json<NewUserEvent>) -> Result<Value, status::Custom<Value>>
    {
        conn.run(move |c| {
            UserRepository::block(current_user, c, new_user_event.into_inner())
                .map(|response| json!(response))
                .map_err(|e| e)
        }).await
    }

    pub async fn follow(current_user: i32, conn: DbConn, new_user_event: Json<NewUserEvent>) -> Result<Value, status::Custom<Value>>
    {
        conn.run(move |c| {
            UserRepository::follow(current_user, c, new_user_event.into_inner())
                .map(|response| json!(response))
                .map_err(|e| e)
        }).await
    }
}