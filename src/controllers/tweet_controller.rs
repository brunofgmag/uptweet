use rocket::serde::{json::Json, json::Value, json::serde_json::json};
use rocket::response::status;
use rocket::http::Status;
use crate::models::{tweet::NewTweet, like::NewLike};
use crate::repositories::tweet_repository::TweetRepository;
use crate::DbConn;

pub struct TweetController;

impl TweetController {
    pub async fn index(user_id: i32, conn: DbConn) -> Result<Value, status::Custom<Value>>
    {
        conn.run(move |c| {
            TweetRepository::all(user_id, c)
                .map(|response| json!(response))
                .map_err(|e| 
                    status::Custom(Status::InternalServerError, json!(e.to_string()))
                )
        }).await
    }

    pub async fn get(id: i32, conn: DbConn) -> Result<Value, status::Custom<Value>>
    {
        conn.run(move |c| {
            TweetRepository::find_by_id(id, c)
                .map(|response| json!(response))
                .map_err(|e| e)
        }).await
    }

    pub async fn create(user_id: i32, conn: DbConn, new_tweet: Json<NewTweet>) -> Result<Value, status::Custom<Value>>
    {
        conn.run(move |c| {
            TweetRepository::create(user_id, c, new_tweet.into_inner())
                .map(|response| json!(response))
                .map_err(|e| e)
        }).await
    }

    pub async fn like(user_id: i32, conn: DbConn, new_like: Json<NewLike>) -> Result<Value, status::Custom<Value>>
    {
        conn.run(move |c| {
            TweetRepository::like(user_id, c, new_like.into_inner())
                .map(|response| json!(response))
                .map_err(|e| e)
        }).await
    }
}