use rocket::serde::{json::Value};
use rocket::response::status;
use rocket::http::Status;
use diesel::result::QueryResult;
use diesel::prelude::*;
use crate::responses::{JsonResponse, ErrorResponse};
use crate::models::{tweet::*, like::*, user_event::UserEvent};
use crate::schema::{tweets, likes};

pub struct TweetRepository;

impl TweetRepository {
    pub fn all(user_id: i32, c: &SqliteConnection) -> QueryResult<Vec<TweetJson>>
    {
        let result = tweets::table.filter(tweets::user_id.eq(user_id)).get_results::<Tweet>(c);

        match result {
            Ok(vec_tweets) => {
                let vec: Vec<TweetJson> = vec_tweets.iter().map(|tweet| {
                    let likes = Self::count_likes(c, &tweet);

                    TweetJson {
                        id: tweet.id,
                        content: tweet.content.to_owned(),
                        user_id: tweet.user_id.to_owned(),
                        tweet_id: tweet.tweet_id.to_owned(),
                        likes: likes,
                        comments: Self::find_comments(tweet.id, c).ok()
                    }
                }).collect();

                Ok(vec)
            },
            Err(e) => Err(e)
        }

    }

    pub fn find_by_id(id: i32, c: &SqliteConnection) -> Result<Value, status::Custom<Value>>
    {
        let result = tweets::table.find(id).first::<Tweet>(c);

        match result {
            Ok(tweet) => {
                let likes = Self::count_likes(c, &tweet);
                Ok(JsonResponse::create(true, Some(200), String::from("OK"), Some(
                    TweetJson::create(tweet.id, tweet.content,tweet.user_id, tweet.tweet_id, likes, Self::find_comments(tweet.id, c).ok())
                )))
            },
            Err(_) => Err(
                ErrorResponse::create(
                    Status::NotFound,
                    404, 
                    "Tweet not found.",
                    None
                )
            )
        }
    }

    pub fn create(user_id: i32, c: &SqliteConnection, new_tweet: NewTweet) -> Result<Value, status::Custom<Value>>
    {
        let result = diesel::insert_into(tweets::table)
            .values((
                tweets::content.eq(new_tweet.content),
                tweets::user_id.eq(user_id),
                tweets::tweet_id.eq(new_tweet.tweet_id),
            ))
            .execute(c);

        match result {
            Ok(_) => {
                let last_id = Self::last_id(c);

                match last_id {
                    Ok(id) => Self::find_by_id(id, c),
                    Err(e) => Err(
                        ErrorResponse::create(
                            Status::InternalServerError,
                            500, 
                            &e.to_string(),
                            None
                        )
                    )
                }
            },
            Err(e) => Err(
                ErrorResponse::create(
                    Status::InternalServerError,
                    500, 
                    &e.to_string(),
                    None
                )
            )
        }
        
    }

    pub fn like(user_id: i32, c: &SqliteConnection, new_like: NewLike) -> Result<Value, status::Custom<Value>>
    {
        if Self::check_for_block(c, user_id, &new_like) {
            Err(
                ErrorResponse::create(
                    Status::Forbidden,
                    403, 
                    "Users cannot interact with eachother.",
                    None
                )
            )
        } else {
            let like_result = Self::find_like(user_id, c, &new_like);

            match like_result {
                Ok(like) => {
                    let result = diesel::delete(likes::table.find(like.id)).execute(c);

                    match result {
                        Ok(_) => Self::find_by_id(new_like.tweet_id, c),
                        Err(_) => Err(
                            ErrorResponse::create(
                                Status::NotFound,
                                404, 
                                "Tweet not found.",
                                None
                            )
                        )
                    }
                },
                Err(_) => {
                    let result = diesel::insert_into(likes::table)
                        .values((
                            likes::tweet_id.eq(&new_like.tweet_id),
                            likes::user_id.eq(user_id)
                        ))
                        .execute(c);

                    match result {
                        Ok(_) => Self::find_by_id(new_like.tweet_id, c),
                        Err(_) => Err(
                            ErrorResponse::create(
                                Status::NotFound,
                                404, 
                                "Tweet not found.",
                                None
                            )
                        )
                    }
                }
            }
        }
    }

    fn check_for_block(c: &SqliteConnection, user_id: i32, new_like: &NewLike) -> bool
    {
        let tweet = tweets::table.find(new_like.tweet_id).first::<Tweet>(c).ok().unwrap();

        let block_exists = UserEvent::exists(
            c,
            user_id,
            tweet.user_id,
            String::from("block")
        );

        match block_exists {
            (_, status) => {
                if status {
                    true
                } else {
                    let block_exists = UserEvent::exists(
                        c,
                        tweet.user_id,
                        user_id,
                        String::from("block")
                    );

                    match block_exists {
                        (_, status) => status
                    }
                }
            }
        }
    }

    fn find_comments(tweet_id: i32, c: &SqliteConnection) -> QueryResult<Vec<TweetJson>>
    {
        let result = tweets::table.filter(tweets::tweet_id.eq(tweet_id)).get_results::<Tweet>(c);

        match result {
            Ok(vec_tweets) => {
                let vec: Vec<TweetJson> = vec_tweets.iter().map(|tweet| {
                    let likes = Self::count_likes(c, &tweet);

                    TweetJson {
                        id: tweet.id,
                        content: tweet.content.to_owned(),
                        user_id: tweet.user_id.to_owned(),
                        tweet_id: tweet.tweet_id.to_owned(),
                        likes: likes,
                        comments: Self::find_comments(tweet.id, c).ok()
                    }
                }).collect();

                Ok(vec)
            },
            Err(e) => Err(e)
        }
    }

    fn count_likes(c: &SqliteConnection, tweet: &Tweet) -> usize
    {
        let result = likes::table
            .filter(likes::tweet_id.eq(tweet.id))
            .get_results::<Like>(c);

        match result {
            Ok(vec) => vec.len(),
            Err(_) => 0,
        }
    }

    fn find_like(user_id: i32, c: &SqliteConnection, new_like: &NewLike) -> QueryResult<Like>
    {
        likes::table
            .filter(likes::user_id.eq(user_id))
            .filter(likes::tweet_id.eq(new_like.tweet_id))
            .first::<Like>(c)
    }

    fn last_id(c: &SqliteConnection) -> QueryResult<i32>
    {
        tweets::table
            .select(tweets::id)
            .order(tweets::id.desc())
            .first(c)
    }
}