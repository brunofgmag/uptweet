use rocket::serde::{json::Value, json::serde_json::json};
use rocket::response::status;
use rocket::http::Status;
use diesel::prelude::*;
use crate::responses::{JsonResponse, ErrorResponse};
use crate::models::{user::*, user_event::UserEvent, user_event::NewUserEvent};
use crate::schema::{users, user_events};

pub struct UserRepository;

impl UserRepository {
    pub fn find(user_id: i32, c: &SqliteConnection) -> Result<Value, status::Custom<Value>>
    {
        let result = users::table.find(user_id).first::<User>(c);

        match result {
            Ok(user) => {
                let followers = Self::count_followers(user.id, c);
                let json_user = UserJson::create(
                    user.id,
                    user.name,
                    user.username,
                    followers
                );

                Ok(JsonResponse::create(true, Some(200), String::from("OK"), Some(json!(json_user))))
            },
            Err(_) => Err(
                ErrorResponse::create(
                    Status::NotFound,
                    404, 
                    "Tweet not found.",
                    None
                )
            )
        }
    }

    pub fn create(c: &SqliteConnection, new_user: NewUser) -> Result<Value, status::Custom<Value>>
    {
        let result = diesel::insert_into(users::table)
            .values(new_user)
            .execute(c);

        match result {
            Ok(_) => {
                let last_id = Self::last_id(c);

                match last_id {
                    Ok(id) => Self::find(id, c),
                    Err(e) => Err(
                        ErrorResponse::create(
                            Status::InternalServerError,
                            500, 
                            &e.to_string(),
                            None
                        )
                    )
                }
            },
            Err(_) => Err(
                ErrorResponse::create(
                    Status::Conflict,
                    409, 
                    "There is another user with this username.",
                    None
                )
            )
        }
    }

    pub fn block(current_user: i32, c: &SqliteConnection, new_user_event: NewUserEvent) -> Result<Value, status::Custom<Value>>
    {
        if current_user == new_user_event.target_id {
            return Err(
                ErrorResponse::create(
                    Status::Forbidden,
                    403, 
                    "You cannot block yourself.",
                    None
                )
            )
        }

        let block_exists = UserEvent::exists(
            c,
            current_user,
            new_user_event.target_id,
            String::from("block")
        );

        match block_exists {
            (id, status) => {
                if status {
                    let result = diesel::delete(user_events::table.find(id)).execute(c);

                    match result {
                        Ok(_) => Ok(JsonResponse::create(true, Some(200), String::from("User unblocked."), None)),
                        Err(e) => Err(
                            ErrorResponse::create(
                                Status::InternalServerError,
                                500, 
                                &e.to_string(),
                                None
                            )
                        )
                    }
                } else {
                    let result = diesel::insert_into(user_events::table)
                    .values((
                        user_events::causer_id.eq(current_user),
                        user_events::target_id.eq(new_user_event.target_id),
                        user_events::event_type.eq(new_user_event.event_type)
                    ))
                    .execute(c);

                    match result {
                        Ok(_) => Ok(JsonResponse::create(true, Some(200), String::from("User blocked."), None)),
                        Err(_) => Err(
                            ErrorResponse::create(
                                Status::NotFound,
                                404, 
                                "User not found.",
                                None
                            )
                        )
                    }
                }
            },
        }
    }

    pub fn follow(current_user: i32, c: &SqliteConnection, new_user_event: NewUserEvent) -> Result<Value, status::Custom<Value>>
    {
        if current_user == new_user_event.target_id {
            return Err(
                ErrorResponse::create(
                    Status::Forbidden,
                    403, 
                    "You cannot follow yourself.",
                    None
                )
            )
        }

        println!("Current user: {} \n Target user: {}", current_user, new_user_event.target_id);

        if Self::check_for_block(c, current_user, new_user_event.target_id) {
            Err(
                ErrorResponse::create(
                    Status::Forbidden,
                    403, 
                    "Users cannot interact with eachother.",
                    None
                )
            )
        } else {
            let follow_exists = UserEvent::exists(
                c,
                current_user,
                new_user_event.target_id,
                String::from("follow")
            );
    
            match follow_exists {
                (id, status) => {
                    if status {
                        let result = diesel::delete(user_events::table.find(id)).execute(c);
    
                        match result {
                            Ok(_) => Ok(JsonResponse::create(true, Some(200), String::from("User unfollowed."), None)),
                            Err(e) => Err(
                                ErrorResponse::create(
                                    Status::InternalServerError,
                                    500, 
                                    &e.to_string(),
                                    None
                                )
                            )
                        }
                    } else {
                        let result = diesel::insert_into(user_events::table)
                        .values((
                            user_events::causer_id.eq(current_user),
                            user_events::target_id.eq(new_user_event.target_id),
                            user_events::event_type.eq(new_user_event.event_type)
                        ))
                        .execute(c);
    
                        match result {
                            Ok(_) => Ok(JsonResponse::create(true, Some(200), String::from("User followed."), None)),
                            Err(_) => Err(
                                ErrorResponse::create(
                                    Status::NotFound,
                                    404, 
                                    "User not found.",
                                    None
                                )
                            )
                        }
                    }
                },
            }
        }
    }

    fn count_followers(current_user: i32, c: &SqliteConnection) -> usize
    {
        let result = user_events::table
            .filter(user_events::target_id.eq(current_user))
            .get_results::<UserEvent>(c);

        match result {
            Ok(vec) => vec.len(),
            Err(_) => 0,
        }
    }

    fn check_for_block(c: &SqliteConnection, user_id: i32, target_id: i32) -> bool
    {
        let block_exists = UserEvent::exists(
            c,
            user_id,
            target_id,
            String::from("block")
        );

        match block_exists {
            (_, status) => {
                if status {
                    true
                } else {
                    let block_exists = UserEvent::exists(
                        c,
                        target_id,
                        user_id,
                        String::from("block")
                    );

                    match block_exists {
                        (_, status) => {
                            status
                        }
                    }
                }
            }
        }
    }

    fn last_id(c: &SqliteConnection) -> QueryResult<i32>
    {
        users::table
            .select(users::id)
            .order(users::id.desc())
            .first(c)
    }
}