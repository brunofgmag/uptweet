use rocket::serde::{json::Json, json::Value};
use rocket::response::status;
use rocket::http::Status;
use crate::responses::ErrorResponse;
use crate::models::{user::UserAuth, tweet::NewTweet, like::NewLike, user::NewUser, user_event::NewUserEvent};
use crate::controllers::{login_controller::LoginController, tweet_controller::TweetController, user_controller::UserController};
use super::auth::BasicAuth;
use super::DbConn;

// Login

#[post("/login", format = "json", data = "<user_auth>")]
pub async fn login(conn: DbConn, user_auth: Json<UserAuth>) -> Result<Value, status::Custom<Value>>
{
    LoginController::login(user_auth.into_inner(), conn).await
}

// Users

#[get("/user")]
pub async fn user_get(auth: BasicAuth, conn: DbConn) -> Result<Value, status::Custom<Value>>
{
    let current_user = auth.user_id;
    UserController::get(current_user, conn).await
}

#[post("/user", format = "json", data = "<new_user>")]
pub async fn user_create(conn: DbConn, new_user: Json<NewUser>) -> Result<Value, status::Custom<Value>>
{
    UserController::create(conn, new_user).await
}

#[post("/user/event", format = "json", data = "<new_user_event>")]
pub async fn user_event_create(auth: BasicAuth, conn: DbConn, new_user_event: Json<NewUserEvent>) -> Result<Value, status::Custom<Value>>
{
    let current_user = auth.user_id;

    if new_user_event.event_type == String::from("block") {
        return UserController::block(current_user, conn, new_user_event).await;
    }

    if new_user_event.event_type == String::from("follow") {
        return UserController::follow(current_user, conn, new_user_event).await;
    }

    Err(
        ErrorResponse::create(
            Status::UnprocessableEntity,
            422, 
            "Invalid event type.",
            None
        )
    )
}

// Tweets

#[get("/tweets")]
pub async fn tweets_index(auth: BasicAuth, conn: DbConn) -> Result<Value, status::Custom<Value>>
{
    let current_user = auth.user_id;
    TweetController::index(current_user, conn).await
}

#[get("/tweets/<id>")]
pub async fn tweets_get(id: i32, _auth: BasicAuth, conn: DbConn) -> Result<Value, status::Custom<Value>>
{
    TweetController::get(id, conn).await
}

#[post("/tweets", format = "json", data = "<new_tweet>")]
pub async fn tweets_create(auth: BasicAuth, conn: DbConn, new_tweet: Json<NewTweet>) -> Result<Value, status::Custom<Value>>
{
    let current_user = auth.user_id;
    TweetController::create(current_user, conn, new_tweet).await
}

#[post("/tweets/like", format = "json", data = "<new_like>")]
pub async fn tweets_like(auth: BasicAuth, conn: DbConn, new_like: Json<NewLike>) -> Result<Value, status::Custom<Value>>
{
    let current_user = auth.user_id;
    TweetController::like(current_user, conn, new_like).await
}