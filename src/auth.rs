use rocket::http::Status;
use rocket::request::{Request, FromRequest, Outcome};
use diesel::prelude::*;
use crate::models::user::User;
use super::schema::*;
use super::DbConn;

#[derive(Debug)]
pub struct BasicAuth {
    pub user_id: i32,
    pub auth_token: String,
}

impl BasicAuth {
    async fn from_authorization_header(header: &str, conn: DbConn) -> Option<BasicAuth> {
        let split = header.split_whitespace().collect::<Vec<_>>();
        if split.len() != 2 {
            return None;
        }

        if split[0] != "Basic" {
            return None;
        }

        Self::from_base64_encoded(String::from(split[1]), conn).await
    }

    async fn from_base64_encoded(token: String, conn: DbConn) -> Option<BasicAuth> {
        let result =  conn
            .run(|c| users::table.filter(users::auth_token.eq(token)).first::<User>(c))
            .await;

        match result {
            Ok(user) => {
                Some(BasicAuth {
                    user_id: user.id,
                    auth_token: user.auth_token.unwrap()
                })
            },
            Err(_) => None
        }

    }
}

#[rocket::async_trait]
impl<'a> FromRequest<'a> for BasicAuth {
    type Error = ();

    async fn from_request(request: &'a Request<'_>) -> Outcome<Self, Self::Error> {
        let conn = request.guard::<DbConn>().await.unwrap();
        let auth_header = request.headers().get_one("Authorization");

        if let Some(auth_header) = auth_header {
            if let Some(auth) = Self::from_authorization_header(auth_header, conn).await {
                return Outcome::Success(auth);
            }
        }

        Outcome::Failure((Status::Unauthorized, ()))
    }
}