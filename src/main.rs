#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_sync_db_pools;
#[macro_use] extern crate diesel;
#[macro_use] extern crate serde;

mod auth;
mod models;
mod repositories;
mod schema;
mod controllers;
mod responses;
mod routes;

use rocket::serde::{json::Value};
use rocket::http::Status;
use rocket::response::status;
use responses::ErrorResponse;

#[database("sqlite_path")]
pub struct DbConn(diesel::SqliteConnection);

#[catch(422)]
fn unprocessable_entity() -> status::Custom<Value> {
    ErrorResponse::create(
        Status::UnprocessableEntity,
        422,
        "Unprocessable entity.",
        None)
}

#[catch(404)]
fn not_found() -> status::Custom<Value> {
    ErrorResponse::create(
        Status::NotFound,
        404,
        "Not found.",
        None)
}

#[catch(401)]
fn unauthorized() -> status::Custom<Value> {
    ErrorResponse::create(
        Status::Unauthorized,
        401,
        "Unauthorized",
        None)
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![
        routes::login,
        routes::user_get,
        routes::user_create,
        routes::user_event_create,
        routes::tweets_index,
        routes::tweets_get,
        routes::tweets_create,
        routes::tweets_like,
    ])
    .register("/", catchers![
        not_found,
        unauthorized,
        unprocessable_entity,
    ])
    .attach(DbConn::fairing())
}